﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;
using System.Collections.Generic;
[RequireComponent(typeof(LineRenderer))]

public class Cartmul : MonoBehaviour
{
    public GameObject camera;
    public GameObject cart;
    public List<Vector3> PositionsVector3s;
    public Transform[] controlPoints;
    public LineRenderer lineRenderer;
    public List<LineRenderer> ls;
    private int curveCount = 0;    
    private int layerOrder = 0;
    private int SEGMENT_COUNT = 50;
    public int index = 0;
    public float speed = 2;
    public float dot = 0;
    private Vector3 next;
    public Transform targetLookAt;
    public float speedCam = 5;
    void Start()
    {
        
        if (!lineRenderer)
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
       
    }


    private void LateUpdate()
    {
        arrocha();
        
    }

    void Update()
    {
       
        //arrocha();
    }

    void arrocha()
    {
        Debug.Log("");
        float m = (controlPoints[index + 1].position - controlPoints[index].position).magnitude;
        float s = (Time.deltaTime * 1 / m) * speed;

        dot +=  s; 
        
        if (dot > 1)
        {
            dot = 0.0f;
            index++;
            
        }
        // else if (dot < 0)
        // {
        //     dot = 1;
        //     index--;
        //     if (index ==  -1)
        //     {
        //        
        //        
        //    }
        //}
        Vector3 pos = CatmullTranslation(index, dot);
        this.next = pos;
        //this.camera.transform.LookAt(pos);
        if (targetLookAt!=null)
        {
            this.camera.transform.LookAt(targetLookAt);    
        }
        
        
        PositionsVector3s.Add(pos);
        lineRenderer.SetPositions(PositionsVector3s.ToArray());
        cart.transform.position = pos;
        //transform.rotation = rail.Orientation(currentIndex, dot);
        
    }
    

    public Vector3 CatmullTranslation (int nodeIndex, float dot)
    {
        Vector3 p1 = new Vector3(), p2 = new Vector3(), p3 = new Vector3(), p4 = new Vector3();

        if(nodeIndex == 0)
        {
            p1 = controlPoints[nodeIndex].position;
            p2 = p1;                                //Small hack to go around array index, this is our last node
            p3 = controlPoints[nodeIndex + 1].position;
            p4 = controlPoints[nodeIndex + 2].position;
        }

        else if(nodeIndex == controlPoints.Length - 2)
        {// evitar erros quando o index esta se aproximando do final
            p1 = controlPoints[nodeIndex - 1].position;
            p2 = controlPoints[nodeIndex].position;
            p3 = controlPoints[nodeIndex + 1].position;
            p4 = p3;
        }
        

        else
        {
            p1 = controlPoints[nodeIndex - 1].position;
            p2 = controlPoints[nodeIndex].position;
            p3 = controlPoints[nodeIndex + 1].position;
            p4 = controlPoints[nodeIndex + 2].position;
        }
        

        float t2 = dot * dot;
        float t3 = t2 * dot;

        float x = 
            0.5f * ((2.0f * p2.x)
                    + (-p1.x + p3.x)
                    * dot + (2.0f * p1.x - 5.0f * p2.x + 4 * p3.x - p4.x)
                    * t2 + (-p1.x + 3.0f * p2.x - 3.0f * p3.x + p4.x)
                    * t3);

        float y =
            0.5f * ((2.0f * p2.y)
                    + (-p1.y + p3.y)
                    * dot + (2.0f * p1.y - 5.0f * p2.y + 4 * p3.y - p4.y)
                    * t2 + (-p1.y + 3.0f * p2.y - 3.0f * p3.y + p4.y)
                    * t3);

        float z =
            0.5f * ((2.0f * p2.z)
                    + (-p1.z + p3.z)
                    * dot + (2.0f * p1.z - 5.0f * p2.z + 4 * p3.z - p4.z)
                    * t2 + (-p1.z + 3.0f * p2.z - 3.0f * p3.z + p4.z)
                    * t3);

        return new Vector3(x, y, z);
    }
    
    
}
