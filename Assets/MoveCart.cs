using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCart : MonoBehaviour
{

    public GameObject[] wayPoints;
    private int index = 0;
    private float speed = 10;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var distance = Vector3.Distance(transform.position, wayPoints[index].transform.position);

        if (distance < 0.1f)
        {
            index++;
        }

        Debug.Log("index: " + index);
        Debug.Log("Distance: " + wayPoints[index].transform.position);

        float step = speed + Time.deltaTime;

        transform.LookAt(wayPoints[index].transform.position);
        transform.position = Vector3.MoveTowards(transform.position, wayPoints[index].transform.position, step);

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
}
